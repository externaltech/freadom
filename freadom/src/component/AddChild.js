import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components'


const ChildMain = styled.div`
margin-top: 5%;
margin-bottom: 5%;

   .Left-child{
       h3{
        
        position: relative;           
        font-weight: normal;
        color: #872fd4;
        font-size: 34px;
        line-height: 34px;
        span{    display: block;
            font-size: 16px;
            padding-left: 20px;
        }
        span:before {
            right: 92%;
            content: "";
            position: absolute;
            top: 23%;
            border: 1px solid #872fd4;
            width: 3%;
            margin: 0 20px;
        }
       }
   }
   .from-group-main{margin-top:40px;
   input{background: #eff6fe;
    border: 0px;
    border-radius: 20px;
    padding: 5px 20px;
    height: 45px;}
}
.form-group{
    
    margin-bottom: 20px;

}
.form-group .btn{
    padding: 10px 30px;
    border-radius: 12px;
    margin-top: 3%;
}
.form-group .btn:nth-child(1){
    background: #ee608d;
    color: #fff;
    margin-right: 20px;
}
.form-group .btn:nth-child(2){
    background: #872fd4;
    color: #fff;
}
.form-group .btn.btn-default.active{
    background:#872fd4;
}
.col-md-5{margin-right:6%;}
`;

class AddChild extends Component {
    componentDidMount() {
		window.scrollTo(0, 0)
	  } 
    render() {
        return (
            <ChildMain>
                <div className="container">
                <div className="row">
                     <div className="col-md-5">
                         <div className="Left-child">
                             <div className="Heading">
                                 <h3><span>ADD YOUR CHILD</span> ENTER INFO</h3>
                             </div>
                             <div className="from-group-main">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Name.."/>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Grade.."/>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="School"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Pincode.."/>
                                </div>
                                <div class="form-group">
                                     <Link to="/booking"><button type="submit" class="btn btn-default">Confirm</button></Link>
                                     <Link to="/booking">  <button type="submit" class="btn btn-default active">Add More</button></Link>
                                </div>
                             </div>
                         </div>
                     </div>
                     
                     <div className="col-md-5">
                         <div className="right-image">
                              <img className="img-responsive" src={process.env.PUBLIC_URL+"img/school.png"} alt={"Child"}/>
                         </div>
                     </div>
                </div>
                </div>
            </ChildMain>
        );
    }
}

export default AddChild;