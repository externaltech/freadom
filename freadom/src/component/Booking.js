import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const BookingSection = styled.div`
  padding-bottom: 70px;
  padding-top: 60px;
  
  .slick-slide, .slick-slide *{ outline: none !important; }
.right-framedate{
    background: #a363ff;
    color: #fff; 
    padding: 10px 10px;
    height:100%;
}
.right-framedate-middle{
    background: #9546ea;
    height: 100%;
    position: relative;
    ul{padding: 0px;}
    li{list-style-type: none;}
    a{  
        color: #fff;
        width: 100%;
        display: inline-block;
        font-size: 18px;
        padding: 5px 10px;
    }
    a:hover{ text-decoration: none;}
    .Add-more{    text-align: center;
        padding-bottom: 10px;
        position: absolute;
        width: 100%;
        bottom: 5px;}
    li a.active{background:#fff; color:#9546ea;}
}
.Zoom-link{text-align: center;
    margin-top: 15px;}
.Zoom-link h4{color:#a8a8a8;font-size: 23px;}
.Zoom-link h4 a{color:#3a99c6;}
.name-date{
    h3{color: #7d12c6;
        font-size: 26px; 
        margin-bottom:35px;
    }
    span{display: block;
        font-weight: normal;}
}
.row_main{box-shadow: 0px 0px 0px 0px #cccccc2e;margin: 5px;border: 5px solid #cccccc0d;}   
.left-main{padding: 10px 10px;}
.bottom-main-section{text-align:right;    margin-bottom: 50px;
    margin-top: 50px;}
.bottom-main-section a{
    border:1px solid #a363ff;
    color:#a363ff;
    font-size: 24px;
    border-radius: 5px;
    padding: 10px 34px;
    text-decoration: none !important;  
    margin-right: 40px;
}
.bottom-main-section a.active{background:#a363ff;    color: #fff;}
.bottom-main-section a:hover{}
.slick-dots li.slick-active button:before {
    opacity: .75;
    color: #872fd4;
    font-size: 18px;
}
.slick-dots li button:before{
    color: #cfacee;
    font-size: 18px;
}
.bottom-main-section.align-center{text-align:center;}
.calendar-api{height:250px;}
`;

class Booking extends Component {
    componentDidMount() {
		window.scrollTo(0, 0)
	  } 
    render() {
        var settings = {
            dots: true,
            infinite: true,
            autoplaySpeed:9000,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
          };
        return (
            <BookingSection>
               <div className="container">
               <Slider {...settings}>
        <div className="main">
            <div className="row row_main">
             <div className="col-md-9">

                 <div className="calendar-api">
                      Calendar Api
                 </div>
                    <div className="name-date">
                         <h3>Thursday<span>November 14, 2019</span></h3>            
                     </div>
                 </div> 
             <div className="col-md-3">
                 <div className="right-framedate">
                     <div className="right-framedate-middle">
                           <ul>
                               <li><Link to="">First kumar</Link></li>
                               <li><Link className="active" to="">Second Kumar</Link></li>
                               <li><Link to="">Third Kumar</Link></li>
                           </ul>
                           <div className="Add-more"><Link to="">Add More +</Link></div>
                     </div>
                 </div>
            </div> 
            </div>
            <div className="col-md-12">
                 <div className="bottom-main-section align-center">
                     <Link className="active" to="#">Confirm Booking</Link>
                 </div>
            </div>
        </div>
        <div>
        <div className="row row_main">
             <div className="col-md-9">
                    <div className="left-main">
                        <div className="row">
                             <div className="col-md-6">
                                 <div className="name-date">
                                     <h3>FIRST KUMAR<span>ID 104568</span></h3>
                                     <h3>Date<span>14 Nov, Thu</span></h3>
                                 </div>
                                
                             </div>
                             <div className="col-md-6">
                             <div className="name-date right">
                                     <h3>TRIAL CLASS DETAILS<span>ID 104568</span></h3>
                                     <h3>Time<span>10:30 AM</span></h3>
                                 </div>
                             </div>
                        </div>
                        <div className="Zoom-link">
                            <h4>Class link</h4>
                            <h4><Link to="">https://us01.zoom.us/123124</Link></h4>
                        </div>
                    </div>
                 </div> 
             <div className="col-md-3">
                 <div className="right-framedate">
                     <div className="right-framedate-middle">
                           <ul>
                               <li><Link className="active" to="">First kumar</Link></li>
                               <li><Link  to="">Second Kumar</Link></li>
                               <li><Link to="">Third Kumar</Link></li>
                           </ul>
                           <div className="Add-more"><Link to="">Add More +</Link></div>
                     </div>
                 </div>
            </div> 
           
            </div>
            <div className="col-md-12">
                 <div className="bottom-main-section">
                     <Link className="active" to="#">Done</Link>  
                     <Link to="#">Reschedule</Link>
                 </div>
            </div>
        </div>
       
      </Slider>
                </div>
            </BookingSection>
        );
    }
}

export default Booking;